import React from "react";
import CircleProgressBar from "../components/progressbar";

const Sidebar = ({ contextState, setContextState }) => {
  const [state, setState] = React.useState(75);

  React.useEffect(() => {
    const data = contextState.series;
    if (data.length === 0) {
      setState(75);
    } else {
      const total = data.reduce((acc, curr) => acc + curr, 0);
      setState(total / data.length);
    }
  }, [contextState]);
  return (
    <div className="sidebar-wrap">
      <div className="sidebar">
        <div className="sidebar-head d-flex justify-content-center">
          <div style={{ width: 200 }}>
            <CircleProgressBar
              trailStrokeColor="#fff"
              strokeColor="teal"
              percentage={state}
              innerText="ACCURACY"
            />
          </div>
        </div>
        <div className="sidebar-body">
          <div className="sidebar-infoCard mb-3">
            <div className="sidebar-infoCard-status">completed</div>
            <div className="sidebar-infoCard-body">
              <div className="sidebar-infoCard-num">99</div>
              <div className="sidebar-infoCard-text ml-2">Problems</div>
            </div>
          </div>
          <div className="sidebar-infoCard">
            <div className="sidebar-infoCard-status">completed</div>
            <div className="sidebar-infoCard-body">
              <div className="sidebar-infoCard-num">20</div>
              <div className="sidebar-infoCard-text ml-2">Problems</div>
            </div>
          </div>
        </div>
      </div>
      <div className="sidebar-foot">
        <p>PAST 24 HOURS, 2000 2/20</p>
      </div>
    </div>
  );
};

export default Sidebar;
