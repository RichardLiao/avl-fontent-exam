import React from "react";
// import useMediaQuery from "@material-ui/core/useMediaQuery";
export const AppContext = React.createContext();

const AppProvider = ({ children }) => {
  // const matchesXS = useMediaQuery("(max-width:767px)");
  const [contextState, setContextState] = React.useState({
    series: [],
  });
  return (
    <AppContext.Provider value={{ contextState, setContextState }}>
      {children}
    </AppContext.Provider>
  );
};

export default AppProvider;
