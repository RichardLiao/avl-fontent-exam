import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TreeView from "@material-ui/lab/TreeView";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import TreeItem from "@material-ui/lab/TreeItem";
import Checkbox from "@material-ui/core/Checkbox";
const useStyles = makeStyles({
  root: {
    height: 240,
    flexGrow: 1,
    maxWidth: 400,
  },
  itemRoot: {
    background: "#fff",
  },
  globalFilterCheckbox: {
    color: "red",
  },
});

export default function Treewithcheckbox({ tree, gteOrgStructure }) {
  const classes = useStyles();
  const [selected, setSelected] = React.useState(new Set([]));
  const [orgStructure, setOrgStructure] = React.useState(tree);
  const checkBoxClickedAll = (event, checked, id) => {
    setOrgStructureElementAll(checked, id, selected, orgStructure);
  };
  const checkBoxClicked = (event, checked, id) => {
    setOrgStructureElement(checked, id, selected, orgStructure);
  };


  const setOrgStructureElementAll = (checked, id, selected, orgStructure) => {
    if (checked) {
      const arr = [];
      orgStructure.forEach((val, idx) => {
        if (val.id === id) {
          val.checked = true;
          // selected.add(val.id);
          setSelected(selected);
          if (val.children) {
            val.children.map((v, vi) => {
              v.checked = true;
              selected.add(v.id);
              setSelected(selected);
              return v;
            });
          }
        }
        arr.push(val);
        setOrgStructure(arr);
        createOrgStructureLevel(arr);
        return val;
      });
    } else {
      const arr = [];
      orgStructure.forEach((val, idx) => {
        if (val.id === id) {
          val.checked = false;
          selected.delete(val.id);
          setSelected(selected);
          if (val.children) {
            val.children.map((v, vi) => {
              v.checked = false;
              selected.delete(v.id);
              setSelected(selected);
              return v;
            });
          }
        }
        arr.push(val);
        setOrgStructure(arr);
        createOrgStructureLevel(arr);
        return val;
      });
    }
  };
  const setOrgStructureElement = (checked, id, selected, orgStructure) => {
    if (checked) {
      selected.add(id);
      setSelected(selected);
      const arr = orgStructure.map((v, i) => {
        if (v.id === id) {
          v.checked = true;
        }
        if (v.children) {
          v.children.map((y, yi) => {
            if (y.id === id) {
              y.checked = true;
            }
            return y;
          });

          const isCheckedOne = v.children.some((v, i) => v.checked === true);
          if (isCheckedOne) {
            v.checked = true;
          }
        }
        return v;
      });
      setOrgStructure(arr);
      createOrgStructureLevel(orgStructure);
    } else {
      selected.delete(id);
      setSelected(selected);
      const arr = orgStructure.map((v, i) => {
        if (v.id === id) {
          v.checked = false;
        }
        if (v.children) {
          v.children.map((y, yi) => {
            if (y.id === id) {
              y.checked = false;
            }
            return y;
          });
          const isCheckedOne = v.children.every((v, i) => v.checked === false);
          if (isCheckedOne) {
            v.checked = false;
          }
        }
        return v;
      });
      setOrgStructure(arr);
      createOrgStructureLevel(orgStructure);
    }
  };

  const createOrgStructureLevel = (val) => {
    const elements = [];

    val.forEach((v, k) => {
      const { id, children } = v;

      if (val.length !== 0) {
        const label = (
          <div className="d-flex align-items-center">
            <Checkbox
              id={`checkbox-${k}`}
              className={classes.globalFilterCheckbox}
              checked={selected.has(id)}
              onChange={(event, checked) => checkBoxClicked(event, checked, id)}
              color="primary"
              onClick={(e) => e.stopPropagation()}
              // indeterminate={true}
            />
            {JSON.stringify(selected.has(id))}
            <div>{v.label}</div>
          </div>
        );
        elements.push(
          // 有 children
          children && children.length > 0 ? (
            <TreeItem
              key={id}
              nodeId={id}
              label={
                <div className="d-flex align-items-center">
                  <Checkbox
                    id={`checkbox-${k}`}
                    className={classes.globalFilterCheckbox}
                    checked={val[k].children.every(
                      (v, i) => v.checked === true
                    )}
                    onChange={(event, checked) =>
                      checkBoxClickedAll(event, checked, id)
                    }
                    color="primary"
                    onClick={(e) => e.stopPropagation()}
                    indeterminate={
                      val[k].children.every((v, i) => v.checked === true)
                        ? false
                        : val[k].children.some((v, i) => v.checked === true)
                        ? true
                        : false
                    }
                  />
                  <div>{v.label}</div>
                </div>
              }
            >
              {createOrgStructureLevel(children)}
            </TreeItem>
          ) : (
            <TreeItem key={id} nodeId={id} label={label} />
          )
        );
      } else if (children) {
        elements.push(createOrgStructureLevel(children));
      }
    });
    gteOrgStructure(orgStructure);
    return elements;
  };
  const addOpenOrgStructurePanel = (e) => {
    // console.log(e);
  };
  const removeOpenOrgStructurePanel = (e) => {
    // console.log(e);
  };
  const handleExpanded = (nodeId, nodeExpanded) => {
    // cache expanded nodes
    if (nodeExpanded) {
      addOpenOrgStructurePanel(nodeId);
    } else {
      removeOpenOrgStructurePanel(nodeId);
    }
  };
  return (
    <TreeView
      className={classes.root}
      onNodeToggle={(nodeId, nodeExpanded) =>
        handleExpanded(nodeId, nodeExpanded)
      }
      defaultCollapseIcon={<ExpandMoreIcon />}
      defaultExpandIcon={<ChevronRightIcon />}
    >
      {orgStructure.length !== 0 ? createOrgStructureLevel(orgStructure) : null}
    </TreeView>
  );
}

// [
//   {
//     id: 1,
//     label: "Arithmetic",
//     checked: false,
//     children: [
//       {
//         id: 2,
//         label: "Subtopic",
//         checked: false,
//       },
//       {
//         id: 3,
//         label: "Subtopic",
//         checked: false,
//       },
//       {
//         id: 4,
//         label: "Subtopic",
//         checked: false,
//       },
//     ],
//   },
//   {
//     id: 5,
//     label: "Arithmetic",
//     checked: false,
//   },
// ]
