import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import clsx from "clsx";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import CloseSharpIcon from "@material-ui/icons/CloseSharp";
import { mainSectionBg } from "../../style/main";
import { borderGradient } from "../../style/main";
const useStyles = makeStyles((theme) => ({
  button: {
    ...borderGradient,
    padding: 0,
    outline: "none",
    "&:hover": {},
    "&:focus": {
      outline: "none",
    },
  },
  ListSubheader: {
    color: "#fff",
  },
  SwipeableDrawer: {
    background: mainSectionBg,
  },
}));

const StyledMenu = withStyles({
  paper: {
    width: "400px",
    marginTop: 0,
    backgroundColor: "black",
    color: "#fff",
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "left",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "left",
    }}
    {...props}
  />
));

function CustomizedMenus(props) {
  const classes = useStyles();
  const matchesXS = useMediaQuery("(max-width:767px)");
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [state, setState] = React.useState({
    bottom: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const handleClick = (event) => {
    if (matchesXS) {
      setState({ ...state, bottom: true });
      return false;
    }
    setState({ ...state, bottom: false });
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === "top" || anchor === "bottom",
      })}
      role="presentation"
      // onClick={toggleDrawer(anchor, false)}
      // onKeyDown={toggleDrawer(anchor, false)}
      style={{
        position: "relative",
        paddingTop: 30,
        paddingBottom: 30,
      }}
    >
      <button
        style={{
          position: "absolute",
          top: 10,
          right: 30,
          border: "none",
          backgroundColor: "transparent",
          color: "#fff",
        }}
        onClick={toggleDrawer(anchor, false)}
      >
        <CloseSharpIcon />
      </button>
      {props.children}
    </div>
  );

  return (
    <div>
      <Button
        aria-controls="customized-menu"
        aria-haspopup="true"
        variant="contained"
        color="primary"
        onClick={handleClick}
        classes={{ root: classes.button }}
      >
        {props.title}
      </Button>
      <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {props.children}
      </StyledMenu>
      <SwipeableDrawer
        anchor={"bottom"}
        open={state["bottom"]}
        onClose={toggleDrawer("bottom", false)}
        onOpen={toggleDrawer("bottom", true)}
        classes={{ paper: classes.SwipeableDrawer }}
      >
        {list("bottom")}
      </SwipeableDrawer>
    </div>
  );
}

export default CustomizedMenus;
