import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
const useStyles = makeStyles((theme) => ({
  label: {
    color: "#fff",
  },
}));

export default function RadioButtonsGroup({
  name,
  title,
  init,
  datas,
  getSelectedValue,
}) {
  const classes = useStyles();
  const [selectedValue, setSelectedValue] = React.useState(init);

  const handleChange = (event) => {
    setSelectedValue(event.target.value);
    getSelectedValue(event.target.value);
  };
  return (
    <div>
      <FormLabel
        component="legend"
        classes={{ root: classes.label }}
        className="mb-3"
      >
        {title}
      </FormLabel>
      <div className="ml-2">
        <RadioGroup
          row
          aria-label="position"
          name="position"
          defaultValue="top"
          value={String(selectedValue)}
        >
          {datas &&
            datas.map((data) => (
              <FormControlLabel
                value={name}
                control={
                  <Radio
                    checked={selectedValue === data.value}
                    onChange={handleChange}
                    value={data.value}
                    name="radio-button-demo"
                    inputProps={{ "aria-label": data.label }}
                  />
                }
                label={
                  <div>
                    {selectedValue === data.value ? (
                      <div className="radio-label active">{data.label}</div>
                    ) : (
                      <div className="radio-label">{data.label}</div>
                    )}
                  </div>
                }
              />
            ))}
        </RadioGroup>
      </div>
    </div>
  );
}
