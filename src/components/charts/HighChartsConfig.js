export default function (obj) {
  return {
    title: {
      text: "Number of Portable Media Players",
    },

    yAxis: {
      title: {
        text: "Number sold (millions)",
      },
    },
    legend: {
      enabled:false,
      layout: "vertical",
      align: "right",
      verticalAlign: "middle",
    },

    plotOptions: {
      series: {
        label: {
          connectorAllowed: false,
        },
        pointStart: 2010,
      },
    },

    series: [
      {
        name: "Installation",
        data: [20, 60, 40, 15, 22, 44, 23, 60],
      },
    ],

    responsive: {
      rules: [
        {
          condition: {
            maxWidth: 500,
          },
          chartOptions: {
            legend: {
              layout: "horizontal",
              align: "center",
              verticalAlign: "bottom",
            },
          },
        },
      ],
    },
    ...obj,
  };
}
