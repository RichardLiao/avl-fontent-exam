export default {
  colors: [
    "#DDDF0D",
    "#55BF3B",
    "#DF5353",
    "#7798BF",
    "#aaeeee",
    "#ff0066",
    "#eeaaee",
    "#55BF3B",
    "#DF5353",
    "#7798BF",
    "#aaeeee",
  ],
  chart: {
    borderRadius:12,
    backgroundColor: "rgba(28,29,33,1)",
    borderColor: "rgba(28,29,33,1)",
    borderWidth: 2,
    className: "dark-container",
    plotBackgroundColor: "rgba(28,29,33,1)",
    plotBorderColor: "rgba(28,29,33,1)",
    plotBorderWidth: 1,
  },
  title: {
    style: {
      color: "#fff",
      font: 'bold 16px "Trebuchet MS", Verdana, sans-serif',
    },
  },
  subtitle: {
    style: {
      color: "#666666",
      font: 'bold 12px "Trebuchet MS", Verdana, sans-serif',
    },
  },
  xAxis: {
    gridLineColor: "#333333",
    gridLineWidth: 0,
    labels: {
      style: {
        color: "#fff",
      },
    },
    lineColor: "#333333",
    tickColor: "#333333",
    title: {
      style: {
        color: "#CCC",
        fontWeight: "bold",
        fontSize: "12px",
        fontFamily: "Trebuchet MS, Verdana, sans-serif",
      },
    },
  },
  yAxis: {
    gridLineColor: "#333333",
    labels: {
      style: {
        color: "#fff",
      },
    },
    lineColor: "#333333",
    minorTickInterval: null,
    tickColor: "#333333",
    tickWidth: 1,
    title: {
      style: {
        color: "rgba(84, 86, 88, 1)",
        fontWeight: "bold",
        fontSize: "15px",
        fontFamily: "Trebuchet MS, Verdana, sans-serif",
      },
    },
  },
  tooltip: {
    backgroundColor: "#fff",
    style: {
      color: "#F0F0F0",
    },
  },
  toolbar: {
    itemStyle: {
      color: "silver",
    },
  },
  plotOptions: {
    series: {
      lineColor: "white",
    },
    bubble: {
      color: "silver",
    },
  },
  legend: {
    backgroundColor: "rgba(0, 0, 0, 1)",
    itemStyle: {
      font: "9pt Trebuchet MS, Verdana, sans-serif",
      color: "#A0A0A0",
    },
    itemHoverStyle: {
      color: "#FFF",
    },
    itemHiddenStyle: {
      color: "#444",
    },
    title: {
      style: {
        color: "#C0C0C0",
      },
    },
  },
  credits: { enabled: false },
  labels: {
    style: {
      color: "#CCC",
    },
  },

  navigation: {
    buttonOptions: {
      symbolStroke: "#DDDDDD",
      hoverSymbolStroke: "#FFFFFF",
      theme: {
        fill: {
          linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
          stops: [
            [0.4, "#606060"],
            [0.6, "#333333"],
          ],
        },
        stroke: "#000000",
      },
    },
  },
};
