import React from "react";
const INITIAL_OFFSET = 25;
const circleConfig = {
  viewBox: "0 0 38 38",
  x: "19",
  y: "19",
  radio: "15.91549430918954",
};

const CircleProgressBarBase = ({
  className,
  trailStrokeColor,
  strokeColor,
  percentage,
  innerText,
}) => {
  return (
    <div style={{ position: "relative" }}>
      <div
        style={{
          position: "absolute",
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <div className="d-flex flex-column align-items-center justify-content-center text-white">
          <div style={{ fontSize: 12, marginBottom: 0 }}> {innerText}</div>
          <span style={{ fontSize: 40 }}>
            {percentage}
            <span
              style={{
                fontSize: 15,
                color: "rgba(84, 86, 88, 1)",
                marginLeft: 4,
              }}
            >
              %
            </span>
          </span>
        </div>
      </div>
      <figure className={className}>
        <svg viewBox={circleConfig.viewBox}>
          <defs>
            <linearGradient id="gradient" x1="0%" y1="0%" x2="0%" y2="100%">
              <stop offset="0%" stop-color="#00bc9b" />
              <stop offset="100%" stop-color="#5eaefd" />
            </linearGradient>
          </defs>
          <circle
            className="ring"
            cx={circleConfig.x}
            cy={circleConfig.y}
            r={circleConfig.radio}
            fill="transparent"
            stroke={trailStrokeColor}
          />

          <circle
            className="path"
            cx={circleConfig.x}
            cy={circleConfig.y}
            r={circleConfig.radio}
            fill="transparent"
            stroke={"url(#gradient)"}
            strokeDasharray={`${percentage} ${100 - percentage}`}
            strokeDashoffset={INITIAL_OFFSET}
          />
        </svg>
      </figure>
    </div>
  );
};

export default CircleProgressBarBase;
