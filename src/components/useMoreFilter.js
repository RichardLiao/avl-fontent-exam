import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Menu from "./material/menu";
import ListSubheader from "@material-ui/core/ListSubheader";
import RadioButtonsGroup from "./material/RadioButtonsGroup";

const useStyles = makeStyles((theme) => ({
  nested: {
    paddingLeft: 0,
  },
  ListSubheader: {
    color: "#fff",
  },
}));

const MoreFilter = () => {
  const classes = useStyles();
  return (

    <Menu title="more filter">
      <ListSubheader
        component="div"
        id="nested-list-subheader"
        classes={{ root: classes.ListSubheader }}
      >
        More Filter
      </ListSubheader>
      <div className="morefilter-wrap px-3">
        <div>
          <RadioButtonsGroup
            name="A"
            title="A section"
            init="a"
            datas={[
              { id: 0, value: "a", label: "A" },
              { id: 1, value: "b", label: "B" },
            ]}
            getSelectedValue={(val) => {
              console.log(val);
            }}
          />
        </div>
        <div className="mt-3">
          <RadioButtonsGroup
            name="B"
            title="B section"
            init="a"
            datas={[
              { id: 0, value: "a", label: "ba" },
              { id: 1, value: "b", label: "bb" },
            ]}
            getSelectedValue={(val) => {
              console.log(val);
            }}
          />
        </div>
      </div>
    </Menu>
  );
};

export default MoreFilter;
