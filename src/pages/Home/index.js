import React from "react";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { fzSmall } from "../../style/main";
import MainTop from "./MainTop";
import withLayout from "../../containers/withLayout";
import ReactHightCharts from "react-highcharts";
import { AppContext } from "../../context";
import HighChartsTheme from "../../components/charts/HighChartsTheme";
import HighChartsConfig from "../../components/charts/HighChartsConfig";
ReactHightCharts.Highcharts.setOptions(HighChartsTheme);

const Home = () => {
  const { contextState, setContextState } = React.useContext(AppContext);
  const matchesXS = useMediaQuery("(max-width:767px)");
  const chartRef = React.useRef();
  const numFormRef = React.useRef();

  const [historical, setHistorical] = React.useState(HighChartsConfig());

  const sendForm = () => {
    setHistorical({
      title: {
        text: "sourceA",
      },
      series: [
        {
          name: "Installation",
          data: [6, 10, 15, 21, 32, 43, 54, 66],
        },
      ],
    });
    setContextState({
      ...contextState,
      series: [6, 10, 15, 21, 32, 43, 54, 66],
    });
  };
  return (
    <div className="main">
      <div className="main-head">
        <MainTop />
      </div>
      <div className="main-body">
        <div className="main-body-top">
          <h3 className="text-yellow">Arithmetic、Real Problem</h3>
        </div>
        <div className="main-body-center">
          <div className="main-body-center-l">
            <p className="text-white" style={{ fontSize: fzSmall }}>
              The recomended daily calcium intake for a 20-year-old is 1,000
              milligrams (mg). One cup of milk contains 299 mg of calcium and
              one cup of juice contains 261 mg of calcium. Which of the
              following inequalities represents the possible number of cups of
              milk m and cups of juice j a 20-year-old counld drink in a day to
              meet or exceed the recommended daily calcium intake from these
              drinks alone ?
            </p>
            <ReactHightCharts
              config={HighChartsConfig(historical)}
              ref={chartRef}
            />
            <div className="d-flex mt-3">
              <button
                className="btn btn-md bg-tiffanyGradient mr-3"
                onClick={() => {
                  setHistorical(HighChartsConfig());
                }}
              >
                初始值
              </button>
              <button
                className="btn btn-md bg-tiffanyGradient mr-3"
                onClick={() => {
                  setHistorical({
                    title: {
                      text: "sourceA",
                    },
                    series: [
                      {
                        name: "Installation",
                        data: [40, 20, 10, 153, 22, 44, 23, 60],
                      },
                    ],
                  });
                }}
              >
                sourceA
              </button>
              <button
                className="btn btn-md bg-tiffanyGradient mr-3"
                onClick={() => {
                  setHistorical({
                    title: {
                      text: "SourceB",
                    },
                    series: [
                      {
                        name: "Installation",
                        data: [6, 6, 66, 66, 22, 44, 23, 12],
                      },
                    ],
                  });
                }}
              >
                SourceB
              </button>
            </div>
          </div>
          <div className="main-body-center-r d-flex align-items-center">
            <form ref={numFormRef} className="d-flex">
              <input
                type="text"
                name="num1"
                className="input-green-outlined mr-2"
                maxLength={1}
                style={{ pointerEvents: "none" }}
                placeholder="1"
                pattern="\d{1,9}"
                onChange={() => {
                  // const value = numFormRef.current.num1.value;
                  // if (value.match(/\d*([.,\/]?\d+)/)) {}
                }}
              />
              <input
                type="text"
                name="num2"
                className="input-green-outlined mr-2"
                maxLength={1}
                style={{ pointerEvents: "none" }}
                placeholder="2"
                value="."
                pattern="\d{1,9}"
              />
              <input
                type="text"
                name="num3"
                className="input-green-outlined mr-2"
                maxLength={1}
                style={{ pointerEvents: "none" }}
                placeholder="3"
                pattern="\d{1,9}"
              />
              <input
                type="text"
                name="num4"
                className="input-green-outlined mr-2"
                maxLength={1}
                style={{ pointerEvents: "none" }}
                placeholder="4"
                pattern="\d{1,9}"
              />
            </form>
          </div>
        </div>
        <div className="main-body-foot">
          <div className="main-body-foot-l"></div>
          <div className="main-body-foot-r">
            <div className="row flex-nowrap">
              <button className="btn btn-circle-tiffanyGradient-outlined">
                Skip
              </button>
              {matchesXS ? null : (
                <button
                  className="btn btn-circle-tiffanyGradient ml-3"
                  onClick={sendForm}
                >
                  Submit
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withLayout(Home);
